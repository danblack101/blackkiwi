﻿using System.Web.Mvc;

namespace BlackKiwi.FrontEnd.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }


        public JsonResult Build(string json)
        {
          //  var result = JsonConvert.DeserializeObject<SystemModel>(json);

            RabbitRapper rapper = new RabbitRapper();
            rapper.PublishMessage(json);

            //var server = MongoServer.Create();
            //var database = server.GetDatabase("test_12345");

            //var collection = database.GetCollection<SystemModel>("SystemDefinition");

            //collection.Insert(result);

       
            //SystemBuilder systemBuilder = new SystemBuilder();
            //systemBuilder.BuildSystem(collection.FindAll().First());
            return Json("Building");
        }

    }
}
