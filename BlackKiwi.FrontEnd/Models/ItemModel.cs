﻿using System.Collections.Generic;

namespace BlackKiwi.FrontEnd.Models
{
    public class ItemModel
    {
        public string name { get; set; }
        public List<FieldModel> fields = new List<FieldModel>();
    }
}