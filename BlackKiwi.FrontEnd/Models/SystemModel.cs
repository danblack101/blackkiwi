﻿using System.Collections.Generic;
using MongoDB.Bson;

namespace BlackKiwi.FrontEnd.Models
{
    public class SystemModel
    {
        public ObjectId Id { get; set; }

        public List<ItemModel> items = new List<ItemModel>();
    }
}