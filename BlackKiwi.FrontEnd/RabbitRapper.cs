﻿using RabbitMQ.Client;
using System.Text;

namespace BlackKiwi.FrontEnd
{
    public class RabbitRapper
    {

        ConnectionFactory _connectionFactory;
        IConnection _connection;
        IModel _channel;
        public RabbitRapper(){
            _connectionFactory = new ConnectionFactory();
            _connection = _connectionFactory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare("BlackKiwi-queue", true, false, false, null);
        }


        public void PublishMessage(string message)
        {
            byte[] messageBytes = Encoding.UTF8.GetBytes(message);
            _channel.BasicPublish(string.Empty, "BlackKiwi-queue", null, messageBytes);
        }
    }
}